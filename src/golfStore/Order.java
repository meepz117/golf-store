/**
 * 
 */
package golfStore;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.Scanner;

import customer.Customer;
import items.golfBall;
import items.golfClub;
import items.poloShirt;

import java.util.Random;

/**
 * @author jmfin
 *
 */
/**
 * @author jmfin
 *
 */
public class Order extends customer.CustomersDemo {
	private Customer customer;
	private double salesTax = -1;
	boolean calculatedShipping = false;
	private double shipping;
	private double subtotal = 0;
	private double total = 0;
	private ShoppingCart cart = new ShoppingCart();
	private Scanner input = new Scanner(System.in);
	
	/**
	 * Gets the current sales tax
	 * @return the salesTax
	 */
	public double getSalesTax() {
		return salesTax;
	}

	/**
	 * Allows the modification of the sales tax
	 * Set to protected for security reasons
	 * @param salesTax the salesTax to set
	 */
	protected void setSalesTax(double salesTax) {
		this.salesTax = salesTax;
	}

	/**
	 * Gets the current shipping cost
	 * @return the shipping
	 */
	public double getShipping() {
		return shipping;
	}

	/**
	 * Allows the modification of the shipping cost.
	 * Set to protected for security reasons.
	 * @param shipping the shipping to set
	 */
	protected void setShipping(double shipping) {
		this.shipping = shipping;
	}

	/**
	 * Gets the current subtotal
	 * @return the subtotal
	 */
	public double getSubtotal() {
		return subtotal;
	}

	/**
	 * Allows the modification of the subtotal.
	 * Set to protected for security reasons.
	 * @param subtotal the subtotal to set
	 */
	protected void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	/**
	 * Gets the current total cost of the cart
	 * @return the total
	 */
	public double getTotal() {
		return total;
	}

	/**
	 * Allows the modification of the total cost.
	 * Set to protected for security reasons.
	 * @param total the total to set
	 */
	protected void setTotal(double total) {
		this.total = total;
	}

	/**
	 * Gets the info of the current customer from the given customer id
	 * @param idNumber The selected customer, uses customer id number
	 */
	public void customerInfo(int idNumber) {		
		for (Customer cust: readCustList()) {
			if (idNumber == Integer.parseInt(cust.getId())) {
				customer = cust;
				break;
			} else {
				continue;
			}
		}
		if (customer == null) {
			System.out.println("Customer not Found");
			return;
		}
	}
	
	/**
	 * Calculates the shipping costs using a random number generator.
	 * Cost Options: Standard ($5-10), Express($10-17)
	 * Only runs rng the first time the method is called
	 */
	public void calculateShipping() {
		Random random = new Random();
		if (!calculatedShipping) {
			String choice = "";
			while (!choice.equals("1") || !choice.equals("2")) {
				System.out.println("Do you want:\n1. Standard Shipping (~$5-9)\n2. Express Shipping (~$10-16)");
				System.out.print("\nDefault \"1\": ");
				try {
					choice = input.next();
					if (choice.isEmpty()) {
						choice = "1";
					}
					switch (Integer.parseInt(choice)) {
					case 1:
						while (shipping < 5.0 || shipping > 10.0) {
							shipping = random.nextInt(15) + random.nextDouble();							
						}
						calculatedShipping = true;
						return;
					case 2:
						while (shipping < 10.0) {
							shipping = random.nextInt(15) + random.nextDouble();
						}
						calculatedShipping = true;
						return;
					
					}
				} catch (NumberFormatException e){
					System.out.println("Invalid Input");
					continue;
				}
				
			}
			
		}		
	}
	
	/**
	 * Calculates the sales tax based on the zipcode.
	 * Zipcode check is not perfect
	 * @param zipCode The customer's zip code
	 */
	public void calculateSalesTax(int zipCode) {
		String stateDesc;
		int zipMin;
		int zipMax;
		double salesTaxPercentage;
		int extraZipMin = 0;
		int extraZipMax = 0;
		// Compares the zipcode to the states.txt to determine the sales tax
		File states = new File("states.txt");
		try {
			Scanner read = new Scanner(states);
			while (read.hasNextLine()) {
				stateDesc = read.nextLine();
				String[] stateInfo = stateDesc.split("\t");
				try {
					if (stateInfo.length >= 4) {
						zipMin = Integer.parseInt(stateInfo[1]);
						zipMax = Integer.parseInt(stateInfo[2]);
						salesTaxPercentage = Double.parseDouble(stateInfo[3]);
						if (stateInfo.length >= 6) {
							extraZipMin = Integer.parseInt(stateInfo[4]);
							extraZipMax = Integer.parseInt(stateInfo[5]);
						}
						if (extraZipMin == 0 && extraZipMax == 0) {
							if (zipCode >= zipMin && zipCode <= zipMax) {
								salesTax = subtotal * salesTaxPercentage;
							} else {
								continue;
							}
						} else {
							if ((zipCode >= zipMin && zipCode <= zipMax) || (zipCode >= extraZipMin && zipCode <= extraZipMax)) {
								salesTax = subtotal * salesTaxPercentage;
							} else {
								continue;
							}
						}
					}
				} catch (NumberFormatException e) {
					System.out.println("Invalid zip value in state list");
					System.exit(0);
				}
				
			}
			if (salesTax == -1) {
				System.out.println("Invalid zip code");
				System.exit(0);
			}
			read.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Calculates the subtotal of every item in the cart
	 */
	public void calculateSubtotal() {
		subtotal = 0;
		for (Item item: cart.shoppingCart) {
			subtotal += Double.parseDouble(item.getPrice()) * Integer.parseInt(item.getQuantity());
		}
		for (poloShirt ps: cart.shirtsCart) {
			subtotal += Double.parseDouble(ps.getPrice());
		}
		for (golfClub gc: cart.golfClubCart) {
			subtotal += Double.parseDouble(gc.getPrice());
		}
		for (golfBall gb: cart.golfBallCart) {
			subtotal += Double.parseDouble(gb.getPrice());
		}
	}
	
	/**
	 * Calculates the total cost of the purchase.
	 * Only calculates on first run or when the cart has
	 * been modified.
	 * @param zipcode The customer's zipcode
	 */
	public void calculateTotal(int zipcode) {
		if (total == 0 || cart.isCartChanged()) {
			calculateSubtotal();
			calculateSalesTax(zipcode);
			calculateShipping();
			total = subtotal + salesTax + shipping;
			cart.setCartChanged(false);
		}
	}
	
	/**
	 * When your done shopping, this will display your cart and the separate costs as well as the total cost.
	 * If the cart is empty, it will declare so and end the method immediately.
	 */
	public void checkOut() {
		if (cart.itemCount > 0) {
			calculateTotal(Integer.parseInt(customer.getShippingAddress().getZip()));
			cart.printCart();
			
			System.out.println("\nSubtotal: $" + String.format("%.2f", subtotal));
			System.out.println("Tax: $" + String.format("%.2f", salesTax));
			System.out.println("Shipping: $" + String.format("%.2f", shipping));
			System.out.println("Total: $" + String.format("%.2f", total));

		} else {
			System.out.println("Your cart is empty.");
		}		
	}
	
	/**
	 * Allows the user to, after inputting their customer id, browse the store catalog,
	 * add and remove items from their shopping cart and, after inputting their correct
	 * credit card info, purchase items from the store.
	 */
	public void choices() {
		String idNumber = null;
		while (customer == null && idNumber != "exit") {
			System.out.print("Input your customer id number or input exit to quit: ");
			idNumber = input.next();
			try {
				if (idNumber.equals("exit")) {
					System.exit(0);
				} else {					
					customerInfo(Integer.parseInt(idNumber));
				}
			} catch (NumberFormatException e) {
				System.out.println("Invalid input");
			}
		}
		
		cart.item.mapItemList();
		int choice = 0;
		int type = 0;
		System.out.println("Hello, " + customer.getCustomerName());
		
		while (choice != 5) {			
			System.out.println("\nWhat do you wish to do?\n1. Look at the Product List\n2. Add item to cart\n3. Remove item from cart\n4. Proceed to checkout\n5. Quit");
			choice = input.nextInt();
			switch (choice) {
			//For view the product list
			case 1:
				cart.item.printList();
				continue;
			/*
			 * For adding an item, by type, to the cart
			 * Quantity only applies to the generic type
			 * May change.
			 * Other types have to be inputted multiple times for more than
			 * one quantity
			 */
			case 2:								
				try {
					System.out.println("Choose item type (1. Generic, 2. Shirt, 3. Clubs, 4. Balls): ");
					type = input.nextInt();
					System.out.println("Input item number:");
					int number = input.nextInt();				
					switch (type) {
					case 1:
						System.out.println("Input quantity:");
						String quantity = input.next();
						Integer.parseInt(quantity);
						cart.addToCart(number, quantity);
						cart.printCart();
						continue;
					case 2:
						cart.addShirtToCart(number);
						cart.printCart();
						continue;
					case 3:
						cart.addClubToCart(number);
						cart.printCart();
						continue;
					case 4:
						cart.addBallToCart(number);
						cart.printCart();
						continue;
					default:
						continue;
					}					
				} catch (NumberFormatException e) {
					System.out.println("Invalid input");
					continue;
				} catch (InputMismatchException e) {
					System.out.println("Invalid input");
					continue;
				}
			/*
			 * For removing an item from the cart.
			 * If the cart is empty, it will declare so with a message and return to the choice menu.
			 * Quantity only applies to the generic type
			 * May change.
			 * Other types have to be inputted multiple times for more than
			 * one quantity
			 */
			case 3:
				if (cart.itemCount > 0) {
					System.out.println("Input item Number:");
					try {
						System.out.println("Choose item type (1. Generic, 2. Shirt, 3. Clubs, 4. Balls): ");
						type = input.nextInt();
						System.out.println("Input item number:");
						int number = input.nextInt();
						switch (type) {
						case 1:
							System.out.println("Input quantity:");
							String quantity = input.next();
							Integer.parseInt(quantity);
							cart.removeFromCart(number, quantity);
							cart.printCart();
							continue;
						case 2:
							cart.removeShirtFromCart(number);
							cart.printCart();
							continue;
						case 3:
							cart.removeClubFromCart(number);
							cart.printCart();
							continue;
						case 4:
							cart.removeBallFromCart(number);
							cart.printCart();
							continue;
						default:
							continue;
						}				
					} catch (NumberFormatException e) {
						System.out.println("Invalid input");
						continue;
					} catch (InputMismatchException e) {
						System.out.println("Invalid input");
						continue;
					}
				} else {
					System.out.println("Your cart is empty.");
					continue;
				}
			/*
			 * For proceeding to checkout.
			 * If the cart is empty it will declare so and return to the choice menu.
			 * If not, it will display your current cart as well as the separate costs and the total cost.
			 * If you wish to purchase, it will ask for your credit card info.
			 * On success, it will thank you and then exit, otherwise it will allow you to try and input the info a few times
			 * before backing out to the choice menu. 
			 */
			case 4:
				checkOut();
				if (cart.itemCount > 0) {
					System.out.println("\nDo you wish to pay now?\n1. Yes\n2. No");
					choice = input.nextInt();
					int tries = 0;
					switch (choice) {
					case 1:
						while (!customer.getCreditCard().isVerified()) {
							customer.getCreditCard().authorize(customer);
							if (tries < 3) {
								if (customer.getCreditCard().isVerified()) {
									System.out.println("Thank you for shopping.");
									System.exit(0);
								} else {
									tries++;
									continue;
								}
							} else {
								System.out.println("Too many incorrect tries. Going back to menu.");
								break;
							}						
						}
						continue;
					case 2:
						continue;
					default:
						continue;
					}
				} else {
					continue;
				}
			case 5:
				System.exit(0);
			default:
				continue;
			}
		}	
	}
}
