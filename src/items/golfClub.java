package items;

/**
 *golfClub class stores stockNumber, name, make, and price. golfClubExtended class extends golfClub class.
 *
 */ 

public class golfClub extends Item

{ 
	// name, make, and price variables are declared for golfClub
	private String golfClubName, golfClubMake;	

/**
 * @param stockNumber
 * @param golfClubName
 * @param golfClubMake
 * @param golfClubPrice
 */
public golfClub(String skueNum, String golfClubName, String golfClubMake, String price)
{   
	super(skueNum, price);
	// constructor has parameters for name, make, and price
  	this.golfClubName = golfClubName;
	this.golfClubMake = golfClubMake;
}


public String toString() 
{
	return "Stock Number: "+ getskueNum() + "      Golf Club Name: " + getgolfClubName() + "      Golf Club Make: " 
			+ getgolfClubMake() + "    Golf Club Price: $" + getPrice();
}
	/**
	 * @return the golfClubName
	 */
	public String getgolfClubName() 
	{
		return golfClubName;
	}
	
	/**
	 * @param golfClubName the golfClubName to set
	 */
	public void setgolfClubName(String golfClubName) 
	{
		this.golfClubName = golfClubName;
	}
	
	/**
	 * @return the golfClubMake
	 */
	public String getgolfClubMake() 
	{
		return golfClubMake;
	}
	
	/**
	 * @param golfClubMake the golfClubMake to set
	 */
	public void setgolfClubMake(String golfClubMake) 
	{
		this.golfClubMake = golfClubMake;
	}
	

	
}