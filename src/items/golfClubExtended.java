package items;

/** 
 * golfClubExtended class extends the golfClub class and stores the golfClubLength
 */

public class golfClubExtended extends golfClub
{	
	//size variable declared for golfClub
	private String golfClubLength;

	/**
	 * @param stockNumber
	 * @param golfClubName
	 * @param golfClubMake
	 * @param golfClubPrice
	 * @param golfClubLength
	 */
	public golfClubExtended(String skueNum, String golfClubName, String golfClubMake, String price, String golfClubLength) 
	{
		super(skueNum, golfClubName, golfClubMake, price);
		
		// sets the golfClubSize
		this.golfClubLength = golfClubLength;		
	}

	/**
	 * @param golfClubLength the golfClubLength to set
	 */
	public void setgolfClubLength(String golfClubLength) 
	{
		this.golfClubLength = golfClubLength;	
	}
	
	/**
	 * @return  the golfClubLength
	 */
	public String getgolfClubLength() 
	{
		return this.golfClubLength;		
	}
	
	/**
	 * toString() is created for stockNumber, golfClub class, displays golfClubName, golfClubMake, golfClubPrice, and golfClubSize
	 */
	public String toString() 
	{	
		return super.toString() + "          Golf Club Length: " + getgolfClubLength();		
	}
}