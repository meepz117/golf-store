package address;
/**
* Project Golf Online 
* author Riddhi Patel
* version 1.0
* Created: 2020-04-09
* Description
*/



import java.io.Externalizable;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.*;
import java.io.ObjectOutput;

/**
 *  we implement Externalizable interface because we want to store this class into a file
 * 
 *
 */

public class Address implements Externalizable {

   /**
    *  by conventions, fields should be private in nature
    */
   private String street;
  
   private String zip;
  
   private String city;
  
   private String state;

   /**
    * getters
    * @return
    */
   public String getStreet() {
       return street;
   }
   

   /**
    *  setters
    * @param street
    */
   public void setStreet(String street) {
       this.street = street;
   }

   public String getZip() {
       return zip;
   }

   public void setZip(String zip) {
       this.zip = zip;
   }

   public String getCity() {
       return city;
   }

   public void setCity(String city) {
       this.city = city;
   }

   public String getState() {
       return state;
   }

   public void setState(String state) {
       this.state = state;
   }
  
   /**
    * your method which prints the address
    */
   public void prettyPrint() {
       System.out.println(this.street + ", " + this.city + ", " + this.state +", " + this.zip);
   }

   /**
    *  we have to write this method because it's a part of Externalizable interface
    * this method describes how you will write the Address into the file
    */
   @Override
   public void writeExternal(ObjectOutput out) throws IOException {
       out.writeObject(street);
       out.writeObject(city);
       out.writeObject(state);
       out.writeObject(zip);
   }

   /**
    *  we have to write this method because it's a part of Externalizable interface
    * this method describes how you will read the Address from a file
    */
   
   public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
       street = (String) in.readObject();
       city = (String) in.readObject();
       state = (String) in.readObject();
       zip = (String) in.readObject();
      
   }

   /** this is needed when we implement Externalizable interface
    * 
    do not remove this, it's a compulsory requirement of Externalizable interface
    */
   /*
    *  John Edit: Removed the system output line because it's annoying having the same
    *  line printed everytime a new object is made
    */
   public Address() {
       super();
   }

   /**
    *  an all-arg constructor
    * @param street
    * @param zip
    * @param city
    * @param state
    */
   public Address(String street, String zip, String city, String state) {
       super();
       this.street = street;
       this.zip = zip;
       this.city = city;
       this.state = state;
   }

  
/**
 *  toString() method
 */
   @Override
   public String toString() {
       return "Address [street=" + street + ", zip=" + zip + ", city=" + city + ", state=" + state + "]";
   }
  
 }
 
