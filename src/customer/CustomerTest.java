package customer;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

import address.Address;

class CustomerTest {
	
	Customer testCustomer;
	
	Address addressMock = mock(Address.class);
	

	@Before
	public void setup() {
	}

	@Test
	void test() {
		String customerId = "1234";
		String customerName = "Jason Knox";
//		Address address = new Address("123 jones street", "bristol", "Wi", "18927");
		
		CreditCard cc = new CreditCard("mastercard","2337-9190-1929-1401","01/23","548");
		testCustomer = new Customer(customerId, customerName, addressMock, cc);
		
		when(addressMock.getStreet()).thenReturn("123 jones street"); 
		when(addressMock.getCity()).thenReturn("bristol");
		when(addressMock.getState()).thenReturn("Wi");
		when(addressMock.getZip()).thenReturn("18927");
		
		
		assertEquals(testCustomer.getId(), customerId);
		assertEquals(testCustomer.getCustomerName(), customerName);
//		assertEquals(testCustomer.getShippingAddress(), address);
		assertEquals(testCustomer.getShippingAddress(), addressMock);
		assertEquals(testCustomer.getCreditCard(), cc);
		
		// wanted but not invoked
		verify(addressMock).getZip();
		//will pass
//		verify(addressMock);
		
	}

}
