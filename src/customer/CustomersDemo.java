package customer;
/**
 * 
 */

/**
 * @author SAM

 *
 */
import java.io.*;     //for PrintWriter class



import java.util.ArrayList;  //for ArrayList class

import java.io.File; //to read from file

import java.io.FileNotFoundException; //for FileNotFoundException

import java.util.Scanner; //for Scanner class

import address.Address;



/**
 * 
 * The CustomersDemo class enables the user
 * to read from the Customer.txt file
 * read ten customer objects
 * and sort them into an array that prints
 * the contents of the file as a String
 * @author SAM
 *
 */

public class CustomersDemo {

	/**
	 * Call the readFile method to read the list
	 * @param args
	 * 
	 */
	public static void main(String[] args) {
		
		/**
		 * call readFile method
		 */
		readFile();

		
		

	}//end main
	
	
	/**
	 * Test reading file
	 * 
	 */
	private static void readFile() {
		/**
		 * Use a try catch block to read from the
		 * customer.txt file
		 */
		try {
			/**
			 * open file you are trying to read
			 */
			File custFile = new File("Customerlist.txt");
			
			/**
			 * create scanner object that is going to read from the file
			 */
			Scanner reader = new Scanner(custFile);
			
			/**
			 * while reading the file line by line
			 */
			while(reader.hasNext()) {
				/**
				 * storing each line into the customer string
				 */
				String newline = reader.nextLine();
				System.out.println(newline);
				
			/**
			 * Split contents and store it into a customers String array
			 */
				String[] customerInput = newline.split(",");
				
				/**
				 * Print out contents of the array
				 */
				for (String s :  customerInput)
					System.out.println(s);
					
								
			} // end while
			
			/**
			 * Always need to close the file
			 */
			reader.close();
			
				
				
			}//end try
		catch(Exception e) {
			System.out.println("The file was not found");
			System.out.println(e.getMessage());
			e.printStackTrace();
			
		}//end catch
		
		
	}//end method
	
	
	
	/**
	 * readCustList enables one to create 
	 * an ArrayList of customers that contain the 
	 * specific fields of both Customer and Address 
	 * classes from reading the Customerlist.txt
	 * file
	 * @return
	 */
	/*
	 *  John Edit: Added protected so it can be accessed outside the class while still being secure
	 *  Also deleted the system outputs as you don't want your customers information being printed
	 *  everytime the arraylist is accessed.
	 */
	protected static ArrayList<Customer> readCustList(){
		
		//Declare a customer ArrayList
		ArrayList<Customer> customerList = new ArrayList<Customer>();
		
		/**
		 * Use a try catch block to read from the
		 * customer.txt file
		 */
		try {//start try
			/**
			 * open file you are trying to read
			 */
			File custFile = new File("Customerlist.txt");
			
			/**
			 * create scanner object that is going to read from the file
			 */
			Scanner reader = new Scanner(custFile);
			
			/**
			 * while reading the file line by line
			 */
			while(reader.hasNextLine()) {//begin while			
				if (reader.hasNextLine()) {
					/**
					 * storing each line into the customer string
					 */
					String newline = reader.nextLine();
					
					/**
					 * Split contents and store it into a customers String array
					 */
					
						String[] theCustomerLine = newline.split(",");
						
										
						/**
						 * Add the contents of the string array
						 * to corresponding fields as a string
						 * print out each element to the console
						 * 
						 */
						String id = theCustomerLine[0];  //id number
						
						
						//customer name
						String name = theCustomerLine[1];
						
						
						
						/**
						 *break out into the address the class.
						 * Account for each field in rhiddi's 
						 * Address objects
						 */
						//street address
						String strAddress = theCustomerLine[2];
						
						
						//city
						String  city = theCustomerLine[3];
						
						
						//state
						String state = theCustomerLine[4];
						
						
						//zip code
						String zip = theCustomerLine[5];
						
						//card issuer
						String issuer = theCustomerLine[6];
						
						//card number
						String cardNumber = theCustomerLine[7];
						
						//card expiration date
						String expDate = theCustomerLine[8];
						
						//card cvv2 number
						String cvv2 = theCustomerLine[9];						
						
						// John Edit: Added this customerAddress variable so cust isn't using a null Address object
						// Edit 2: Added this card variable for the newly finished CreditCard class
						Address customerAddress = new Address(strAddress, zip, city, state);
						CreditCard card = new CreditCard(issuer, cardNumber, expDate, cvv2);
						
						/**
						 * Creating customer object and storing the customer attributes
						 */
						Customer cust = new Customer(id, name, customerAddress, card);
						/**
						 * add customer object to the arrayList
						 */
						customerList.add(cust);	
				}
			} // end while
			
			/**
			 * close the file
			 */
			reader.close();
			
				
				
			}//end try
		
		/**
		 * File not found exception to catch when a file is not found
		 */
		catch(FileNotFoundException e) {
			System.out.println("The file was not found");
			e.printStackTrace();
		
		}//end catch
		/**
		 * catch when the Array contents go past what can be held
		 */
		catch(ArrayIndexOutOfBoundsException e) {
			System.out.println(customerList.toString());
			e.printStackTrace();
		}//end catch
		
		/**
		 * return statement
		 * @return customerList
		 */
		return customerList;
			
	}//end main
	
	// John's Edit: Moved this outside the readCustList() method so it isn't run everytime readCustList() is called
	protected void printCustList() {
		/**
		 * for each customer in the ArrayList, print out the contents as a string
		 */
		for(Customer cust: readCustList()) {
			System.out.println(cust.toString());
			
		}//end for
	}
	
	


}//end class
